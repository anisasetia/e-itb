<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barang_user', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->bigInteger('barang_id')->unsigned();
            $table->foreign('barang_id')
                ->references('id')->on('barangs')
                ->onDelete('cascade');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            // $table->foreignId('barang_id')->constrained('barang');
            // $table->foreignId('akun_id')->constrained('users');
            // $table->primary(['barang_id', 'akun_id']);

            // $table->foreign('barang_id')->references('id')->on('barang')->onDelete('cascade');
            // $table->foreign('akun_id')->references('id')->on('akun')->onDelete('cascade');
            // $table->bigIncrements('id');

            $table->string('kode_transaksi', 45)->nullable();
            $table->string('status', 45)->nullable();
            $table->string('bukti_bayar', 225)->nullable();
            $table->integer('total_bayar')->nullable();
            $table->integer('qty')->nullable();
            // // $table->date('tanggal_transaksi');
            // $table->unsignedBigInteger('akun_id');
            // $table->foreign('akun_id')->references('id')->on('users');
            // $table->unsignedBigInteger('barang_id');
            // $table->foreign('barang_id')->references('id')->on('barang');
            // $table->unsignedBigInteger('admin_id');
            // $table->foreign('admin_id')->references('id')->on('admin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi');
    }
}
