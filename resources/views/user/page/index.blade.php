@extends('user.layout.master')
@section('title', 'Home')
@section('header')
<div class="hero-area hero-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 offset-lg-2 text-center">
                <div class="hero-text">
                    <div class="hero-text-tablecell">
                        <p class="subtitle">E-book</p>
                        <h1>Menyediakan E-book IT</h1>
                        <div class="hero-btns">
                            <a href="{{ route('product') }}" class="boxed-btn">E-book Collection</a>
                            <a href="contact.html" class="bordered-btn">Contact Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="list-section pt-80 pb-80">
    <div class="container">

        <div class="row">
            <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
                <div class="list-box d-flex align-items-center">
                    <div class="list-icon">
                        <i class="fas fa-shipping-fast"></i>
                    </div>
                    <div class="content">
                        <h3>Free Shipping</h3>
                        <p>When order over $75</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
                <div class="list-box d-flex align-items-center">
                    <div class="list-icon">
                        <i class="fas fa-phone-volume"></i>
                    </div>
                    <div class="content">
                        <h3>24/7 Support</h3>
                        <p>Get support all day</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="list-box d-flex justify-content-start align-items-center">
                    <div class="list-icon">
                        <i class="fas fa-sync"></i>
                    </div>
                    <div class="content">
                        <h3>Refund</h3>
                        <p>Get refund within 3 days!</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- end features list section -->

<!-- product section -->
<div class="product-section mt-150 mb-150">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2 text-center">
                <div class="section-title">
                    <h3><span class="orange-text">Our</span> Products</h3>
                    <p>E-ITb menyediakan bermacam-macam buku tentang Teknik Informatika. Mulai dari bahasa pemrograman,
                        Artificial Intelegence, IoT, Security (Keamanan Cyber) dan masih banyak lagi.</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-6 text-center">
                <div class="single-product-item">
                    <div class="product-image">
                        <a href="single-product.html"><img src="{{ asset('user/assets/img/products/laravel1.png') }}"
                                alt=""></a>
                    </div>
                    <h3>Laravel</h3>
                    <p class="product-price"><span>Per item</span> 85$ </p>
                    <a href="cart.html" class="cart-btn"><i class="fas fa-shopping-cart"></i> Add to Cart</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 text-center">
                <div class="single-product-item">
                    <div class="product-image">
                        <a href="single-product.html"><img src="{{ asset('user/assets/img/products/kotlin.jpg') }}"
                                alt=""></a>
                    </div>
                    <h3>Kotlin</h3>
                    <p class="product-price"><span>Per item</span> 70$ </p>
                    <a href="cart.html" class="cart-btn"><i class="fas fa-shopping-cart"></i> Add to Cart</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 offset-md-3 offset-lg-0 text-center">
                <div class="single-product-item">
                    <div class="product-image">
                        <a href="single-product.html"><img src="{{ asset('user/assets/img/products/phyton1.png') }}"
                                alt=""></a>
                    </div>
                    <h3>Phyton</h3>
                    <p class="product-price"><span>Per item</span> 65$ </p>
                    <a href="cart.html" class="cart-btn"><i class="fas fa-shopping-cart"></i> Add to Cart</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end product section -->

<!-- cart banner section -->
<section class="cart-banner pt-100 pb-100">
    <div class="container">
        <div class="row clearfix">
            <!--Image Column-->
            <div class="image-column col-lg-6">
                <div class="image">
                    <div class="price-box">
                        <div class="inner-price">
                            <span class="price">
                                <strong>30%</strong> <br> off per item
                            </span>
                        </div>
                    </div>
                    <img src="{{ asset('user/assets/img/a1.png') }}" alt="">
                </div>
            </div>
            <!--Content Column-->
            <div class="content-column col-lg-6">
                <h3><span class="orange-text">Deal</span> of the month</h3>
                <h4>Laravel</h4>
                <div class="text">Buku ini menjelaskan tentang bagaimana belajar Laravel dengan cepat dan mudah.</div>
                <!--Countdown Timer-->
                <div class="time-counter">
                    <div class="time-countdown clearfix" data-countdown="2020/2/01">
                        <div class="counter-column">
                            <div class="inner"><span class="count">00</span>Days</div>
                        </div>
                        <div class="counter-column">
                            <div class="inner"><span class="count">00</span>Hours</div>
                        </div>
                        <div class="counter-column">
                            <div class="inner"><span class="count">00</span>Mins</div>
                        </div>
                        <div class="counter-column">
                            <div class="inner"><span class="count">00</span>Secs</div>
                        </div>
                    </div>
                </div>
                <a href="cart.html" class="cart-btn mt-3"><i class="fas fa-shopping-cart"></i> Add to Cart</a>
            </div>
        </div>
    </div>
</section>
<!-- end cart banner section -->

<!-- testimonail-section -->
<div class="testimonail-section mt-150 mb-150">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1 text-center">
                <div class="testimonial-sliders">
                    <div class="single-testimonial-slider">
                        <div class="client-avater">
                            <img src="{{ asset('user/assets/img/avaters/avatar1.png') }}" alt="">
                        </div>
                        <div class="client-meta">
                            <h3>Tya <span>Local student</span></h3>
                            <p class="testimonial-body">
                                " Senang bisa berbelanja di E-ITb selain harganya yang terjangkau juga buku-buku yang
                                disediakan cukup banyak. "
                            </p>
                            <div class="last-icon">
                                <i class="fas fa-quote-right"></i>
                            </div>
                        </div>
                    </div>
                    <div class="single-testimonial-slider">
                        <div class="client-avater">
                            <img src="{{ asset('user/assets/img/avaters/avatar2.png') }}" alt="">
                        </div>
                        <div class="client-meta">
                            <h3>Darmawan <span>Local student</span></h3>
                            <p class="testimonial-body">
                                " Udah langganan kalau cari e-book pasti ke E-ITb. Selain harganya yang ramah kantong
                                juga pelayanannya yang cepat. "
                            </p>
                            <div class="last-icon">
                                <i class="fas fa-quote-right"></i>
                            </div>
                        </div>
                    </div>
                    <div class="single-testimonial-slider">
                        <div class="client-avater">
                            <img src="{{ asset('user/assets/img/avaters/avatar3.png') }}" alt="">
                        </div>
                        <div class="client-meta">
                            <h3>Ahmad <span>Local student</span></h3>
                            <p class="testimonial-body">
                                " Pokoknya ga kecewa dehh kalau beli e-book di E-ITb, banyak kejutan dan discount. "
                            </p>
                            <div class="last-icon">
                                <i class="fas fa-quote-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end testimonail-section -->

<!-- advertisement section -->
<div class="abt-section mb-150">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <div class="abt-bg">
                    <a href="https://www.youtube.com/watch?v=kWP_Eu6meT0" class="video-play-btn popup-youtube"><i
                            class="fas fa-play"></i></a>
                </div>
                <!--https://www.youtube.com/watch?v=DBLlFWYcIGQ -->
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="abt-text">
                    <p class="top-sub">Since Year 2021</p>
                    <h2>We are <span class="orange-text">E-ITb</span></h2>
                    <p>E-Itb pertama kali dirintis oleh Andika, Annisa dan Ismail saat sedang mengikuti program bootcamp
                        dari PKS Digital School. Dimana E-ITb ini adalah project pertama yang dirilis sebagai tugas
                        bulanan. Berawal dari sinilah kami ingin menjadikan E-ITb tidak hanya sekedar menjadi sebuah
                        project tanpa implementasi nyata. Sehingga kami bertiga sepakat untuk menjadikan E-ITb sebagai
                        mini e-commerce.</p>
                    <p>Harapan kami E-ITb bisa membantu para mahasiswa, dosen maupun masyarakat umum yang mencari e-book
                        dan buku tentang Teknik Informatika dengan mudah dan cepat, selain juga e-book yang terupdate
                        dan sedang menjadi trending topic dikalangan developer.</p>
                    <a href="about.html" class="boxed-btn mt-4">know more</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end advertisement section -->

<!-- shop banner -->
<section class="shop-banner">
    <div class="container">
        <h3>December sale is on! <br> with big <span class="orange-text">Discount...</span></h3>
        <div class="sale-percent"><span>Sale! <br> Upto</span>50% <span>off</span></div>
        <a href="shop.html" class="cart-btn btn-lg">Shop Now</a>
    </div>
</section>
<!-- end shop banner -->

<!-- latest news -->
<div class="latest-news pt-150 pb-150">
    <div class="container">

        <div class="row">
            <div class="col-lg-8 offset-lg-2 text-center">
                <div class="section-title">
                    <h3><span class="orange-text">Our</span> News</h3>
                    <p>Kemudahan yang E-ITb sediakan merupakan wujud pelayanan kami yang ingin memanjakan pelanggannya
                        saat berbelanja di toko kami. </p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="single-latest-news">
                    <a href="single-news.html">
                        <div class="latest-news-bg news-bg-1"></div>
                    </a>
                    <div class="news-text-box">
                        <h3><a href="single-news.html">Anda akan menemukan keseruan sendiri saat membaca e-book di toko
                                kami.</a></h3>
                        <p class="blog-meta">
                            <span class="author"><i class="fas fa-user"></i> Admin</span>
                            <span class="date"><i class="fas fa-calendar"></i> 11 November, 2021</span>
                        </p>
                        <p class="excerpt">Penasaran dengan ringkasan e-book di toko kami, silahkan untuk buat akun
                            sebagai pelanggan kami.</p>
                        <a href="single-news.html" class="read-more-btn">read more <i
                                class="fas fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-latest-news">
                    <a href="single-news.html">
                        <div class="latest-news-bg news-bg-2"></div>
                    </a>
                    <div class="news-text-box">
                        <h3><a href="single-news.html">Membangun E-ITb dengan kerjasama dan kerja keras dalam waktu 5
                                hari.</a></h3>
                        <p class="blog-meta">
                            <span class="author"><i class="fas fa-user"></i> Admin</span>
                            <span class="date"><i class="fas fa-calendar"></i> 11 November, 2021</span>
                        </p>
                        <p class="excerpt">E-ITb menjadi pilihan banyak mahasiswa dan dosen sehingga cocok sekali
                            dijadikan referensi mencari e-book yang digunakan untuk perkuliahan. </p>
                        <a href="single-news.html" class="read-more-btn">read more <i
                                class="fas fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 offset-md-3 offset-lg-0">
                <div class="single-latest-news">
                    <a href="single-news.html">
                        <div class="latest-news-bg news-bg-3"></div>
                    </a>
                    <div class="news-text-box">
                        <h3><a href="single-news.html">Melayani dengan teknologi.</a></h3>
                        <p class="blog-meta">
                            <span class="author"><i class="fas fa-user"></i> Admin</span>
                            <span class="date"><i class="fas fa-calendar"></i> 11 November, 2019</span>
                        </p>
                        <p class="excerpt">E-ITb merupakan e-book store dengan konsep "Melayani dengan Teknologi".
                            Konsep ini kami usung sebagai bentuk pelayanan di toko kami yang memudahkan pelanggan dengan
                            pemanfaatan teknologi saat ini.</p>
                        <a href="single-news.html" class="read-more-btn">read more <i
                                class="fas fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center">
                <a href="news.html" class="boxed-btn">More News</a>
            </div>
        </div>
    </div>
</div>
<!-- end latest news -->

<!-- logo carousel -->
<div class="logo-carousel-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="logo-carousel-inner">
                    <div class="single-logo-item">
                        <img src="{{ asset('user/assets/img/company-logos/logo1.png') }}" alt="">
                    </div>
                    <div class="single-logo-item">
                        <img src="{{ asset('user/assets/img/company-logos/logo2.png') }}" alt="">
                    </div>
                    <div class="single-logo-item">
                        <img src="{{ asset('user/assets/img/company-logos/logo3.png') }}" alt="">
                    </div>
                    <div class="single-logo-item">
                        <img src="{{ asset('user/assets/img/company-logos/logo4.png') }}" alt="">
                    </div>
                    <div class="single-logo-item">
                        <img src="{{ asset('user/assets/img/company-logos/logo5.png') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end logo carousel -->

@endsection
