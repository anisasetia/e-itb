@extends('user.layout.master')
@section('header')
<!-- breadcrumb-section -->
<div class="breadcrumb-section breadcrumb-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2 text-center">
                <div class="breadcrumb-text">
                    <p>List E-book</p>
                    <h1>Product</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end breadcrumb section -->

<!-- products -->
@endsection
@section('content')
<div class="product-section mt-150 mb-150">
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <div class="product-filters">
                    <ul>
                        <li class="active" data-filter="*">All</li>
                        @forelse ($kategoris as $kategori)
                        <li data-filter=".{{ $kategori->nama }}">{{ $kategori->nama }}</li>
                        @empty
                        @endforelse
                    </ul>
                </div>
            </div>
        </div>

        <div class="row product-lists">
            @forelse ($ebooks as $ebook)
            <div class="col-lg-4 col-md-6 text-center {{ $ebook->kategori->nama }}">
                <div class="single-product-item">
                    <div class="product-image">
                        <a href="{{ route('detail',$ebook->id) }}"><img class=""
                                src="{{ Storage::url($ebook->gambar) }}" alt=""></a>
                    </div>
                    <h3>{{ $ebook->nama_brg }}</h3>
                    <p class="product-price"><span>Harga</span> {{number_format($ebook->harga) }} </p>
                    <a href="{{ route('detail',$ebook->id) }}" class="cart-btn">
                        Lihat</a>
                </div>
            </div>

            @empty

            @endforelse
        </div>

    </div>


    <div class="row">
        <div class="col-lg-12 text-center">
            <div class="pagination-wrap">
                <ul>
                    <li><a href="#">Prev</a></li>
                    <li><a href="#">1</a></li>
                    <li><a class="active" href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">Next</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
</div>
<!-- end products -->

{{-- <div class="row product-lists">
    @forelse ($ebooks as $ebook)
    <form action="{{ route('user.buy') }}" method="POST">
        @csrf
        <input type="hidden" name="tanggal_transaksi" value="{{ $now }}">
        <input type="hidden" name="tanggal_transaksi" value="{{ $ebook->nama_brg }}">
        <input type="hidden" readonly name="harga" value="{{ $ebook->harga }}">
        <input type="hidden" name="qty" value="{{ $ebook->stok }}">

        <input type="hidden" name="kode_transaksi" value="{{ $nomor }}">
        <input type="hidden" name="status" value="pending">
    </form>
    <div class="col-lg-4 col-md-6 text-center {{ $ebook->nama_brg }}">
        <div class="single-product-item">
            <div class="product-image">
                <a href="single-product.html"><img src="assets/img/products/product-img-2.jpg" alt=""></a>
            </div>
            <h3>{{ $ebook->nama_brg }}</h3>
            <p class="product-price"><span></span> {{ $ebook->harga }} </p>
            <p><span>Stok</span>{{ $ebook->stok }}</p>
            <a href="cart.html" class="cart-btn"><i class="fas fa-shopping-cart"></i> Add to Cart</a>
        </div>
    </div>
    @empty

    @endforelse

</div>
</div> --}}

<!-- logo carousel -->
<div class="logo-carousel-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="logo-carousel-inner">
                    <div class="single-logo-item">
                        <img src="{{ asset('user/assets/img/company-logos/1.png') }}" alt="">
                    </div>
                    <div class="single-logo-item">
                        <img src="{{ asset('user/assets/img/company-logos/2.png') }}" alt="">
                    </div>
                    <div class="single-logo-item">
                        <img src="{{ asset('user/assets/img/company-logos/3.png') }}" alt="">
                    </div>
                    <div class="single-logo-item">
                        <img src="{{ asset('user/assets/img/company-logos/4.png') }}" alt="">
                    </div>
                    <div class="single-logo-item">
                        <img src="{{ asset('user/assets/img/company-logos/5.png') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end logo carousel -->
@endsection
