@extends('user.layout.master')
@section('header')
<!-- breadcrumb-section -->
<div class="breadcrumb-section breadcrumb-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2 text-center">
                <div class="breadcrumb-text">
                    <p>Programming Book</p>
                    <h1>Check Out Product</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end breadcrumb section -->
@endsection
@section('content')
<!-- cart -->
<div class="cart-section mt-150 mb-150">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="cart-table-wrap">
                    <table class="cart-table">
                        <thead class="cart-table-head">
                            <tr class="table-head-row">
                                <th>No Transaksi</th>
                                <th>Nama Barang</th>
                                <th>qty</th>
                                <th>Total Bayar</th>
                                <th>Status</th>

                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($cart as $item)
                            <tr class="table-body-row">
                                <td>{{ $item->kode_transaksi }}</td>
                                <td>{{ $item->barang }}</td>
                                <td>{{ $item->qty }}</td>
                                <td>{{ $item->total_bayar }}</td>
                                <td>{{ $item->status }}</td>


                                @empty
                                <td colspan="6">Tidak ada transaksi</td>
                                @endforelse

                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>


        </div>
    </div>
</div>
<!-- end cart -->

@endsection
