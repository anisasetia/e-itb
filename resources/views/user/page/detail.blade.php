@extends('user.layout.master')
@section('header')
<!-- breadcrumb-section -->
<div class="breadcrumb-section breadcrumb-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2 text-center">
                <div class="breadcrumb-text">
                    <p>Detail</p>
                    <h1>{{ $detail->nama_brg }}</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end breadcrumb section -->
@endsection
@section('content')

<div class="abt-section mb-150 mt-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <div class="detail-book">
                    <img src="{{ Storage::url($detail->gambar) }}" class="card-img-top" alt="...">
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="abt-text">
                    <p class="top-sub">{{ $detail->tanggal }}</p>
                    <h2>Ebook <span class="orange-text">{{ $detail->nama_brg }}</span></h2>
                    <p>{{ $detail->deskripsi }}</p>
                    <form action="{{ route('user.buy') }}" method="POST">
                        @csrf
                        <input type="hidden" name="barang_id" value="{{ $detail->id }}">
                        <input type="hidden" name="nama_brg" value="{{ $detail->nama_brg }}">
                        <input type="hidden" name="kode_transaksi" value="{{ $nomor }}">
                        <input type="hidden" readonly name="total_bayar" value="{{ $detail->harga }}">
                        <input type="hidden" name="qty" value="1">
                        <input type="hidden" name="bukti_bayar" value="nih">
                        <input type="hidden" name="akun_id" value="{{ Auth::user()->id }}">

                        <input type="hidden" name="status" value="pending">

                        <input type="submit" class="boxed-btn mt-4" value="Order now">
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
