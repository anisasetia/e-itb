<div class="loader">
    <div class="loader-inner">
        <div class="circle"></div>
    </div>
</div>
<div class="top-header-area" id="sticker">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 text-center">
                <div class="main-menu-wrap">
                    <!-- logo -->
                    <div class="site-logo">
                        <a href="{{ route('home') }}">
                            <img style="width: 454px; hight: 131px ;" src="{{ asset('user/assets/img/E-ITb.png') }}"
                                alt="">
                        </a>
                    </div>
                    <!-- logo -->

                    <!-- menu start -->
                    <nav class="main-menu">
                        <ul>
                            <li class="{{ request()->is('/') ? 'current-list-item' : '' }}">
                                <a href="/">Home</a>
                            </li>
                            <li class="{{ request()->is('about') ? 'current-list-item' : '' }}">
                                <a href="{{ route('about') }}">About</a>
                            </li>
                            <li class="{{ request()->is('product') ? 'current-list-item' : '' }}">
                                <a href="{{ route('product') }}">Ebook</a>
                            </li>

                            <li class="">
                                <div class="header-icons">
                                    <a class="shopping-cart" href="{{ route('cart') }}">
                                        <i
                                            class="fas fa-shopping-cart {{ request()->is('cart') ? 'color-icon' : '' }}"></i>
                                    </a>
                                    @guest
                                    <a class="" href="{{ route('login') }}"><i class="fas fa-sign-in-alt">&nbsp
                                            Login</i></a>
                                    @endguest

                                    @auth
                                    <a class="" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                        <i class="fas fa-sign-in-alt"></i>
                                        Logout
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                        style="display: none">
                                        @csrf
                                    </form>
                                </div>
                </div>
                @endauth

            </div>
            </li>
            </ul>

            </nav>
            <a class="mobile-show search-bar-icon" href="#"><i class="fas fa-search"></i></a>
            <div class="mobile-menu"></div>
            <!-- menu end -->
        </div>
    </div>
</div>
</div>
</div>
<!-- end header -->
