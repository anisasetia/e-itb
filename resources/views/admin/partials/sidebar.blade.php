<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <div class="text-center sidebar-brand-wrapper d-flex align-items-center">
        <a class="sidebar-brand brand-logo" href="index.html"><img src="{{ asset('user/assets/img/E-ITb.png') }}"
                alt="logo" /></a>
    </div>
    <ul class="nav">
        <li class="nav-item nav-profile">
            <a href="#" class="nav-link">
                <div class="nav-profile-image">
                    <img src="{{asset('adminMaster/template/assets/images/faces/face2.jpg')}}" alt="profile" />
                    <span class="login-status online"></span>
                    <!--change to offline or busy as needed-->
                </div>
                <div class="nav-profile-text d-flex flex-column pr-3">
                    @guest
                    <span class="font-weight-medium mb-2">Guest</span>
                    <span class="font-weight-normal">viewer</span>
                    @endguest
                    @auth
                    <span class="font-weight-medium mb-2">{{ Auth::user()->name }}</span>
                    <span class="font-weight-normal">Full Control</span>
                    @endauth

                </div>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/admin">
                <i class="mdi mdi-home menu-icon"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>

        <li class="nav-item">

            <a class="nav-link" href="/daftar-barang">
                <i class="fa fa-archive menu-icon"></i>

                <span class="menu-title">Daftar Buku</span>
            </a>
            <a class="nav-link" href="/kategori">
                <i class="fa fa-archive menu-icon"></i>

                <span class="menu-title">Daftar Kategori</span>
            </a>

            <a class="nav-link" href="/transaksi">
                <i class="fa fa-archive menu-icon"></i>

                <span class="menu-title">Daftar Transaksi</span>
            </a>

            <a class="nav-link" href="/akun">
                <i class="fa fa-archive menu-icon"></i>

                <span class="menu-title">Daftar Akun</span>
            </a>

            <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                <i class="mdi mdi-logout mr-2 text-danger"></i>
                Logout
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none">
                @csrf
            </form>

        </li>
    </ul>
</nav>
