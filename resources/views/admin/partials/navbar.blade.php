<nav class="navbar col-lg-12 col-12 p-lg-0 fixed-top d-flex flex-row">
    <div class="navbar-menu-wrapper d-flex align-items-stretch justify-content-between">
        <a class="navbar-brand brand-logo-mini align-self-center d-lg-none" href="index.html"><img
                src="{{asset('adminMaster/template/assets/images/logo-mini.svg')}}" alt="logo" /></a>
        <button class="navbar-toggler navbar-toggler align-self-center mr-2" type="button" data-toggle="minimize">
            <i class="mdi mdi-menu"></i>
        </button>
        <ul class="navbar-nav">
            {{-- kode --}}
        </ul>
        <ul class="navbar-nav navbar-nav-right ml-lg-auto">
            <li class="nav-item nav-profile dropdown border-0">
                <a class="nav-link dropdown-toggle" id="profileDropdown" href="#" data-toggle="dropdown">
                    @guest
                    <img class="nav-profile-img mr-2" alt=""
                        src="{{asset('adminMaster/template/assets/images/faces/face2.jpg')}}" />
                    <span class="profile-name">Guest</span>
                    @endguest

                    @auth
                    <img class="nav-profile-img mr-2" alt=""
                        src="{{asset('adminMaster/template/assets/images/faces/face1.jpg')}}" />
                    <span class="profile-name">{{ Auth::user()->name }}</span>
                    @endauth
                </a>
                <div class="dropdown-menu navbar-dropdown w-100" aria-labelledby="profileDropdown">
                    <a class="dropdown-item" href="#">
                        <i class="mdi mdi-account-box mr-2 text-dark"></i> Profile </a>

                    @guest
                    <a class="dropdown-item" href="{{ route('login') }}">
                        <i class="mdi mdi-login mr-2 text-dark"></i> Login </a>
                    @endguest

                    @auth
                    <a class="dropdown-item" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                        <i class="mdi mdi-logout mr-2 text-danger"></i> 
                        Logout
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none">
                        @csrf
                    </form>
                    @endauth

                </div>
            </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button"
            data-toggle="offcanvas">
            <span class="mdi mdi-menu"></span>
        </button>
    </div>
</nav>
