@extends('admin.layouts.master')

@section('title')
| Daftar Transaksi
@endsection

@section('mainJudul')
Daftar Transaksi
@endsection

@section('subJudul')

@endsection

@push('dataTablesCSS')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css" />
@endpush



@section('content1')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="col-lg grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Transaksi Table</h4>
            <form action="{{ url('import') }}" method="POST" name="importform" enctype="multipart/form-data">
                @csrf

                <div class="form-group">
                    <a class="btn btn-info" href="{{ route('export_trans') }}">Export File</a>
                </div>

            </form>
            <a href="{{ route('export_trans') }}">Export</a>

            <table id="tableBarang" class="table table-hover">
                <thead class="">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Kode Transaksi</th>
                        <th scope="col">Nama Barang</th>
                        <th scope="col">Qty</th>
                        <th scope="col">Total Bayar</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody class="table-active">

                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
@push('dataTablesJS')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.js"></script>
<script>
    var oTable ;
    $(document).ready(function () {
        oTable = $('#tableBarang').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: {
                url : "{{ route('list_data')}}",
                // data:{
                //     ticket_id : ticketId
                // }
            },
            columns: [
                { data: 'id',
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },

                {data : 'kode_transaksi', name: 'kode_transaksi'},
                {data : 'nama_barang', name: 'nama_barang'},
                {data : 'qty', name: 'qty'},
                {data : 'total_bayar', name: 'total_bayar'},
                {data : 'status', name: 'status'},

                { data: 'id',
                    render: function (data, type, row) {
                        let buttonEdit = '<button class="btn btn-icon btn-icon-only btn-primary" type="button" onclick="buttonApprove(\''+data+'\');" style="width: 60px;">Approve</button>';
                        let buttonDelete = '<button class="btn btn-icon btn-icon-only btn-danger" type="button" onclick="buttonDelete(\''+data+'\');" style="width: 60px;">Delete</button>';
                        return buttonEdit+buttonDelete;
                    }
                }

            ]
        });
    });

function buttonDelete(idx){
    $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "DELETE",
                    url: "{{ url('transaksi')}}"+"/"+idx,
                    success: function(datax) {
                        oTable.ajax.reload();
                    }
                });
}
function buttonApprove(idx){
    $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "PUT",
                    url: "{{ url('transaksi')}}"+"/"+idx,
                    success: function(datax) {
                        oTable.ajax.reload();
                    }
                });
}

</script>


@endpush
