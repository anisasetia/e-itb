@extends('admin.layouts.master')

@section('title')
| Daftar Barang
@endsection

@section('mainJudul')
Daftar Barang
@endsection

@section('subJudul')

@endsection

@push('dataTablesCSS')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css" />
@endpush

@push('dataTablesJS')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.js"></script>
<script>
    $(function () {
      $("#tableBarang").DataTable();
    });
</script>
@endpush

@section('content1')

<div class="col-lg grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Kategori Table</h4>
            <a href="/kategori/create" type="button" class="btn btn-success mb-3">Tambah Kategori</a>
            <table id="tableBarang" class="table table-hover">
                <thead class="">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody class="table-active">
                    @forelse ($kategori as $key => $data)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$data->nama}}</td>
                        <td>
                            <form action="/kategori/{{$data->id}}" method="POST">
                                <a href="/kategori/{{$data->id}}" type="button"
                                    class="btn btn-primary btn-sm">Detail</a>
                                <a href="/kategori/{{$data->id}}/edit" type="button"
                                    class="btn btn-warning btn-sm mx-1">Edit</a>
                                @csrf
                                @method('DELETE')
                                <button name="submit" type="submit" class="btn btn-danger btn-sm">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td>Null</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
