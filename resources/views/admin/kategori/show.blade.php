@extends('admin.layouts.master')

@section('title')
| Detail Katgori
@endsection

@section('mainJudul')
Detail Kategori
@endsection

@section('subJudul')

@endsection

@section('content1')
<h5 class="col-2 font-weight-bold">Nama Kategori</h5>
<h5 class="col-10">: {{$kategori->nama}}</h5>

<a href="/daftar-barang" type="button" class="btn btn-secondary mt-3">Kembali</a>
@endsection
