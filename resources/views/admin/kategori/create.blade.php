@extends('admin.layouts.master')

@section('title')
| Tambah Barang
@endsection

@section('mainJudul')
Tambah Barang
@endsection

@section('subJudul')

@endsection

@section('content1')
<div class="col-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tambah Kategori</h4>
            <p class="card-description"></p>
            <form class="forms-sample" action="/kategori" method="POST">
                @csrf
                <div class="form-group">
                    <label for="exampleInputName1">Name</label>
                    <input type="text" name="nama" class="form-control" id="exampleInputName1"
                        placeholder="Nama Kategori" />
                </div>

                <button type="submit" class="btn btn-primary mr-2"> Submit </button>

            </form>
        </div>
    </div>
</div>
@endsection
