@extends('admin.layouts.master')

@section('title')
| Edit Kategori
@endsection

@section('mainJudul')
Edit Kategori
@endsection

@section('subJudul')

@endsection

@section('content1')
<form action="/kategori/{{$kategori->id}}" method="POST">
    @csrf
    @method('PUT')

    <div class="form-group row">
        <label for="kode" class="col-2 col-form-label">Nama Kategori</label>
        <div class="col-10">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fa fa-address-book"></i>
                    </div>
                </div>
                <input value="{{$kategori->nama}}" id="kode" name="nama" type="text" class="form-control">
            </div>
            @error('nama')
            <div class="alert alert-warning small">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="offset-2 col-9">
            <button name="submit" type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>
@endsection
