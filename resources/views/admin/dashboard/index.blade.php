@extends('admin.layouts.master')

@section('title')
    | Dashboard
@endsection

@section('mainJudul')
Hi, welcome back!
@endsection

@section('subJudul')
Your web analytics dashboard template.
@endsection

@section('content1')
<div class="col-xl-3 col-lg-12 stretch-card grid-margin">
    <div class="row">
        <div class="col-xl-12 col-md-6 stretch-card grid-margin grid-margin-sm-0 pb-sm-3">
            <div class="card bg-warning">
                <div class="card-body px-3 py-4">
                    <div class="d-flex justify-content-between align-items-start">
                        <div class="color-card">
                            <p class="mb-0 color-card-head">Sales</p>
                            <h2 class="text-white"> $8,753.<span class="h5">00</span>
                            </h2>
                        </div>
                        <i class="card-icon-indicator mdi mdi-basket bg-inverse-icon-warning"></i>
                    </div>
                    <h6 class="text-white">18.33% Since last month</h6>
                </div>
            </div>
        </div>
        <div class="col-xl-12 col-md-6 stretch-card grid-margin grid-margin-sm-0 pb-sm-3">
            <div class="card bg-danger">
                <div class="card-body px-3 py-4">
                    <div class="d-flex justify-content-between align-items-start">
                        <div class="color-card">
                            <p class="mb-0 color-card-head">Margin</p>
                            <h2 class="text-white"> $5,300.<span class="h5">00</span>
                            </h2>
                        </div>
                        <i class="card-icon-indicator mdi mdi-cube-outline bg-inverse-icon-danger"></i>
                    </div>
                    <h6 class="text-white">13.21% Since last month</h6>
                </div>
            </div>
        </div>
        <div class="col-xl-12 col-md-6 stretch-card grid-margin grid-margin-sm-0 pb-sm-3 pb-lg-0 pb-xl-3">
            <div class="card bg-primary">
                <div class="card-body px-3 py-4">
                    <div class="d-flex justify-content-between align-items-start">
                        <div class="color-card">
                            <p class="mb-0 color-card-head">Orders</p>
                            <h2 class="text-white"> $1,753.<span class="h5">00</span>
                            </h2>
                        </div>
                        <i class="card-icon-indicator mdi mdi-briefcase-outline bg-inverse-icon-primary"></i>
                    </div>
                    <h6 class="text-white">67.98% Since last month</h6>
                </div>
            </div>
        </div>
        <div class="col-xl-12 col-md-6 stretch-card pb-sm-3 pb-lg-0">
            <div class="card bg-success">
                <div class="card-body px-3 py-4">
                    <div class="d-flex justify-content-between align-items-start">
                        <div class="color-card">
                            <p class="mb-0 color-card-head">Affiliate</p>
                            <h2 class="text-white">2368</h2>
                        </div>
                        <i class="card-icon-indicator mdi mdi-account-circle bg-inverse-icon-success"></i>
                    </div>
                    <h6 class="text-white">20.32% Since last month</h6>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-xl-9 stretch-card grid-margin">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-7">
                    <h5>Business Survey</h5>
                    <p class="text-muted"> Show overview jan 2018 - Dec 2019 <a
                            class="text-muted font-weight-medium pl-2" href="#"><u>See Details</u></a>
                    </p>
                </div>
                <div class="col-sm-5 text-md-right">
                    <button type="button" class="btn btn-icon-text mb-3 mb-sm-0 btn-inverse-primary font-weight-normal">
                        <i class="mdi mdi-file-pdf btn-icon-prepend"></i>Download Report PDF </button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content2')
<div class="col-xl-4 col-md-6 grid-margin stretch-card">
    <div class="card card-invoice">
        <div class="card-body">
            <h4 class="card-title pb-3">Pending invoices</h4>
            <div class="list-card">
                <div class="row align-items-center">
                    <div class="col-7 col-sm-8">
                        <div class="row align-items-center">
                            <div class="col-sm-4">
                                <img src="{{asset('adminMaster/template/assets/images/faces/face2.jpg')}}" alt="" />
                            </div>
                            <div class="col-sm-8 pr-0 pl-sm-0">
                                <span>06 Jan 2019</span>
                                <h6 class="mb-1 mb-sm-0">Isabel Cross</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-5 col-sm-4">
                        <div class="d-flex pt-1 align-items-center">
                            <div class="reload-outer bg-info">
                                <i class="mdi mdi-reload"></i>
                            </div>
                            <div class="dropdown dropleft pl-1 pt-3">
                                <div id="dropdownMenuButton2" data-toggle="dropdown" role="button" aria-haspopup="true"
                                    aria-expanded="false">
                                    <p><i class="mdi mdi-dots-vertical"></i></p>
                                </div>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                                    <a class="dropdown-item" href="#">Sales</a>
                                    <a class="dropdown-item" href="#">Track Invoice</a>
                                    <a class="dropdown-item" href="#">Payment History</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="list-card">
                <div class="row align-items-center">
                    <div class="col-7 col-sm-8">
                        <div class="row align-items-center">
                            <div class="col-sm-4">
                                <img src="{{asset('adminMaster/template/assets/images/faces/face3.jpg')}}" alt="" />
                            </div>
                            <div class="col-sm-8 pr-0 pl-sm-0">
                                <span>18 Mar 2019</span>
                                <h6 class="mb-1 mb-sm-0">Carrie Parker</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-5 col-sm-4">
                        <div class="d-flex pt-1 align-items-center">
                            <div class="reload-outer bg-primary">
                                <i class="mdi mdi-reload"></i>
                            </div>
                            <div class="dropdown dropleft pl-1 pt-3">
                                <div id="dropdownMenuButton3" data-toggle="dropdown" role="button" aria-haspopup="true"
                                    aria-expanded="false">
                                    <p><i class="mdi mdi-dots-vertical"></i></p>
                                </div>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton3">
                                    <a class="dropdown-item" href="#">Sales</a>
                                    <a class="dropdown-item" href="#">Track Invoice</a>
                                    <a class="dropdown-item" href="#">Payment History</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="list-card">
                <div class="row align-items-center">
                    <div class="col-7 col-sm-8">
                        <div class="row align-items-center">
                            <div class="col-sm-4">
                                <img src="{{asset('adminMaster/template/assets/images/faces/face11.jpg')}}" alt="" />
                            </div>
                            <div class="col-sm-8 pr-0 pl-sm-0">
                                <span>10 Apr 2019</span>
                                <h6 class="mb-1 mb-sm-0">Don Bennett</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-5 col-sm-4">
                        <div class="d-flex pt-1 align-items-center">
                            <div class="reload-outer bg-warning">
                                <i class="mdi mdi-reload"></i>
                            </div>
                            <div class="dropdown dropleft pl-1 pt-3">
                                <div id="dropdownMenuButton4" data-toggle="dropdown" role="button" aria-haspopup="true"
                                    aria-expanded="false">
                                    <p><i class="mdi mdi-dots-vertical"></i></p>
                                </div>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton4">
                                    <a class="dropdown-item" href="#">Sales</a>
                                    <a class="dropdown-item" href="#">Track Invoice</a>
                                    <a class="dropdown-item" href="#">Payment History</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="list-card">
                <div class="row align-items-center">
                    <div class="col-7 col-sm-8">
                        <div class="row align-items-center">
                            <div class="col-sm-4">
                                <img src="{{asset('adminMaster/template/assets/images/faces/face3.jpg')}}" alt="" />
                            </div>
                            <div class="col-sm-8 pr-0 pl-sm-0">
                                <span>18 Mar 2019</span>
                                <h6 class="mb-1 mb-sm-0">Carrie Parker</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-5 col-sm-4">
                        <div class="d-flex pt-1 align-items-center">
                            <div class="reload-outer bg-info">
                                <i class="mdi mdi-reload"></i>
                            </div>
                            <div class="dropdown dropleft pl-1 pt-3">
                                <div id="dropdownMenuButton5" data-toggle="dropdown" role="button" aria-haspopup="true"
                                    aria-expanded="false">
                                    <p><i class="mdi mdi-dots-vertical"></i></p>
                                </div>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton5">
                                    <a class="dropdown-item" href="#">Sales</a>
                                    <a class="dropdown-item" href="#">Track Invoice</a>
                                    <a class="dropdown-item" href="#">Payment History</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-xl-4 grid-margin stretch-card">
    <!--activity-->
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <span class="d-flex justify-content-between">
                    <span>Activity</span>
                    <span class="dropdown dropleft d-block">
                        <span id="dropdownMenuButton1" data-toggle="dropdown" role="button" aria-haspopup="true"
                            aria-expanded="false">
                            <span><i class="mdi mdi-dots-horizontal"></i></span>
                        </span>
                        <span class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <a class="dropdown-item" href="#">Contact</a>
                            <a class="dropdown-item" href="#">Helpdesk</a>
                            <a class="dropdown-item" href="#">Chat with us</a>
                        </span>
                    </span>
                </span>
            </h4>
            <ul class="gradient-bullet-list border-bottom">
                <li>
                    <h6 class="mb-0"> It's awesome when we find a new solution </h6>
                    <p class="text-muted">2h ago</p>
                </li>
                <li>
                    <h6 class="mb-0">Report has been updated</h6>
                    <p class="text-muted">
                        <span>2h ago</span>
                        <span class="d-inline-block">
                            <span class="d-flex d-inline-block">
                                <img class="ml-1" src="{{asset('adminMaster/template/assets/images/faces/face1.jpg')}}"
                                    alt="" />
                                <img class="ml-1" src="{{asset('adminMaster/template/assets/images/faces/face10.jpg')}}"
                                    alt="" />
                                <img class="ml-1" src="{{asset('adminMaster/template/assets/images/faces/face14.jpg')}}"
                                    alt="" />
                            </span>
                        </span>
                    </p>
                </li>
                <li>
                    <h6 class="mb-0"> Analytics dashboard has been created#Slack </h6>
                    <p class="text-muted">2h ago</p>
                </li>
                <li>
                    <h6 class="mb-0"> It's awesome when we find a new solution </h6>
                    <p class="text-muted">2h ago</p>
                </li>
            </ul>
            <a class="text-black mt-3 mb-0 d-block h6" href="#">View all <i class="mdi mdi-chevron-right"></i></a>
        </div>
    </div>
    <!--activity ends-->
</div>
@endsection
