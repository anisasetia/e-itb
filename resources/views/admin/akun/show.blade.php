@extends('admin.layouts.master')

@section('title')
| Detail Akun
@endsection

@section('mainJudul')
Detail Akun
@endsection

@section('subJudul')

@endsection

@section('content1')
<h5 class="col-2 font-weight-bold">Kode</h5>
<h5 class="col-10">: {{$akuns->name}}</h5>
<h5 class="col-2 font-weight-bold">Email</h5>
<h5 class="col-10">: {{$akuns->email}}</h5>
<h5 class="col-2 font-weight-bold">Role</h5>
<h5 class="col-10">: {{$akuns->role}}</h5>

<a href="/daftar-barang" type="button" class="btn btn-secondary mt-3">Kembali</a>
@endsection
