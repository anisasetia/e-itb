@extends('admin.layouts.master')

@section('title')
| Daftar Akun
@endsection

@section('mainJudul')
Daftar Akun
@endsection

@section('subJudul')

@endsection

@push('dataTablesCSS')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css" />
@endpush

@push('dataTablesJS')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.js"></script>
<script>
    $(function () {
    $("#tableBarang").DataTable();
    });
</script>
@endpush

@section('content1')

<div class="col-lg grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Akun Table</h4>
            <a href="/akun/create" type="button" class="btn btn-success mb-3">Tambah Akun</a>
            <table id="tableBarang" class="table table-hover">
                <thead class="">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Email</th>
                        <th scope="col">Role</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody class="table-active">
                    @forelse ($akuns as $key => $data)

                    @if ($data->role == 0)
                    {{ $data->role = "user" }}

                    @else
                    {{ $data->role = "admin" }}

                    @endif

                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$data->name}}</td>
                        <td>{{$data->email}}</td>
                        <td>{{ $data->role }}</td>

                        <td>
                            <form action="/akun/{{$data->id}}" method="POST">
                                <a href="/akun/{{$data->id}}" type="button" class="btn btn-primary btn-sm">Detail</a>
                                <a href="/akun/{{$data->id}}/edit" type="button"
                                    class="btn btn-warning btn-sm mx-1">Edit</a>
                                @csrf
                                @method('DELETE')
                                <button name="submit" type="submit" class="btn btn-danger btn-sm">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td>Null</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
