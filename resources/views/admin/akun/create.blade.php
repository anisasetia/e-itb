@extends('admin.layouts.master')

@section('title')
Buat Akun
@endsection

@section('mainJudul')
BuatAkun
@endsection

@section('subJudul')

@endsection

@push('dataTablesCSS')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css" />
@endpush

@push('dataTablesJS')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.js"></script>
<script>
    $(function () {
      $("#tableBarang").DataTable();
    });
</script>
@endpush

@section('content1')
<div class="col-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tambah Akun</h4>
            <p class="card-description"></p>
            <form class="forms-sample" action="/akun" method="POST">
                @csrf
                <div class="form-group">
                    <label for="exampleInputName1">Nama</label>
                    <input type="text" name="name" class="form-control" id="exampleInputName1" placeholder="Nama" />
                </div>

                <div class="form-group">
                    <label for="exampleInputName1">Email</label>
                    <input type="email" name="email" class="form-control" id="exampleInputName1" placeholder="Email" />
                </div>

                <div class="form-group">
                    <label for="exampleInputName1">Password</label>
                    <input id="password" name="password" type="password" class="form-control">
                </div>

                <div class="form-group">
                    <label for="role">Role</label>

                    <select name="role" id="role" class="form-control">
                        <option value="">---Pilih Role---</option>
                        <option value="0">User</option>
                        <option value="1">Admin</option>
                    </select>

                </div>

                <button type="submit" class="btn btn-primary mr-2"> Daftar </button>

            </form>
        </div>
    </div>
</div>
@endsection
