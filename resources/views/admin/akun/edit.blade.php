@extends('admin.layouts.master')

@section('title')
| Edit Akun
@endsection

@section('mainJudul')
Edit Akun
@endsection

@section('subJudul')

@endsection

@section('content1')
<form action="/akun/{{$akuns->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group row">
        <label for="kode" class="col-2 col-form-label">Kode</label>
        <div class="col-10">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fa fa-address-book"></i>
                    </div>
                </div>
                <input value="{{$akuns->name}}" id="kode" name="name" type="text" class="form-control">
            </div>
        </div>
    </div>



    <div class="form-group row">
        <label for="harga" class="col-2 col-form-label">Role</label>
        <div class="col-10">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fa fa-address-book"></i>
                    </div>
                </div>
                <select name="role" id="role" class="form-control">
                    <option value="">---Pilih Role---</option>
                    <option value="0">User</option>
                    <option value="1">Admin</option>
                </select>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="offset-2 col-9">
            <button name="submit" type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>
@endsection
