@extends('admin.layouts.master')

@section('title')
| Detail Barang
@endsection

@section('mainJudul')
Detail Barang
@endsection

@section('subJudul')

@endsection

@section('content1')
<h5 class="col-2 font-weight-bold">Kode</h5>
<h5 class="col-10">: {{$barang->kode}}</h5>
<h5 class="col-2 font-weight-bold">Image</h5>
<h5 class="col-10">:
    <div class="card" style="width: 18rem;">
        <img src="{{ Storage::url($barang->gambar) }}" class="card-img-top" alt="...">
    </div>
</h5>
<h5 class="col-2 font-weight-bold">Nama Barang</h5>
<h5 class="col-10">: {{$barang->nama_brg}}</h5>
<h5 class="col-2 font-weight-bold">Harga</h5>
<h5 class="col-10">: {{$barang->harga}}</h5>
<h5 class="col-2 font-weight-bold">Stok</h5>
<h5 class="col-10">: {{$barang->stok}}</h5>
<h5 class="col-2 font-weight-bold">Deskripsi</h5>
<h5 class="col-10">: {{$barang->deskripsi}}</h5>
<h5 class="col-2 font-weight-bold">Penulis</h5>
<h5 class="col-10">: {{$barang->penulis}}</h5>
<h5 class="col-2 font-weight-bold">Penerbit</h5>
<h5 class="col-10">: {{$barang->penerbit}}</h5>
<h5 class="col-2 font-weight-bold">Date</h5>
<h5 class="col-10">: {{$barang->tanggal}}</h5>
<a href="/daftar-barang" type="button" class="btn btn-secondary mt-3">Kembali</a>
@endsection
