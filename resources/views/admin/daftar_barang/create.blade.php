@extends('admin.layouts.master')

@section('title')
Input Barang
@endsection

@section('mainJudul')
Input Buku
@endsection

@section('subJudul')

@endsection

@push('dataTablesCSS')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css" />
@endpush

@push('dataTablesJS')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.js"></script>
<script>
    $(function () {
      $("#tableBarang").DataTable();
    });
</script>
@endpush

@section('content1')
<div class="col-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tambah Buku</h4>
            <p class="card-description"></p>
            <form class="forms-sample" action="/daftar-barang" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="exampleInputName1">Kode Buku</label>
                    <input type="text" name="kode" class="form-control" id="exampleInputName1"
                        placeholder="Kode Buku" />
                </div>

                <div class="form-group">
                    <label for="exampleInputName1">Name</label>
                    <input type="text" name="nama_brg" class="form-control" id="exampleInputName1"
                        placeholder="Nama Buku" />
                </div>

                <div class="form-group">
                    <label for="exampleInputName1">Image</label>
                    <input id="gambar" name="gambar" type="file" class="form-control">
                </div>

                <div class="form-group">
                    <label for="exampleInputName1">Harga</label>
                    <input type="number" name="harga" class="form-control" id="exampleInputName1"
                        placeholder="Nama Buku" />
                </div>

                <div class="form-group">
                    <label for="exampleInputName1">Stok</label>
                    <input type="number" name="stok" class="form-control" id="exampleInputName1"
                        placeholder="Stok Buku" />
                </div>

                <div class="form-group">
                    <label for="exampleInputName1">Deskripsi</label>
                    <textarea id="deskripsi" name="deskripsi" cols="40" rows="4" class="form-control"></textarea>
                </div>

                <div class="form-group">
                    <label for="exampleInputName1">Penulis</label>
                    <input type="text" name="penulis" class="form-control" id="exampleInputName1"
                        placeholder="Nama Penulis" />
                </div>

                <div class="form-group">
                    <label for="exampleInputName1">Penerbit</label>
                    <input type="text" name="penerbit" class="form-control" id="exampleInputName1"
                        placeholder="Nama Penerbit" />
                </div>

                <div class="form-group">
                    <label for="exampleInputName1">Tanggal terbit</label>
                    <div class="col-10">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fa fa-address-book"></i>
                                </div>
                            </div>
                            <input id="tanggal" name="tanggal" type="date" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="kategori_id" class="">Kategori</label>
                    <div class="col-10">
                        <select name="kategori_id" id="kategori_id" class="form-control">
                            <option value="">---Pilih Kategori---</option>
                            @foreach ($kategori as $item)
                            <option value="{{ $item->id }}">{{ $item->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary mr-2"> Daftar </button>

            </form>
        </div>
    </div>
</div>
@endsection
