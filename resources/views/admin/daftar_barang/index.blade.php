@extends('admin.layouts.master')

@section('title')
| Daftar Barang
@endsection

@section('mainJudul')
Daftar Barang
@endsection

@section('subJudul')

@endsection

@push('dataTablesCSS')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css" />
@endpush

@push('dataTablesJS')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.js"></script>
<script>
    $(function () {
      $("#tableBarang").DataTable();
    });
</script>
@endpush

@section('content1')

<div class="col-lg grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Basic Table</h4>
            <a href="/daftar-barang/create" type="button" class="btn btn-success mb-3">Tambah Data</a>
            <table id="tableBarang" class="table table-hover">
                <thead class="">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Kode</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Harga</th>
                        <th scope="col">Stok</th>
                        <th scope="col">Kategori</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody class="table-active">
                    @forelse ($barang as $key => $data)

                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$data->kode}}</td>
                        <td>{{$data->nama_brg}}</td>
                        <td>{{number_format($data->harga)}}</td>
                        <td>{{$data->stok}}</td>
                        <td>{{$data->kategori->nama}}</td>
                        <td>
                            <form action="/daftar-barang/{{$data->id}}" method="POST">
                                <a href="/daftar-barang/{{$data->id}}" type="button"
                                    class="btn btn-primary btn-sm">Detail</a>
                                <a href="/daftar-barang/{{$data->id}}/edit" type="button"
                                    class="btn btn-warning btn-sm mx-1">Edit</a>
                                @csrf
                                @method('DELETE')
                                <button name="submit" type="submit" class="btn btn-danger btn-sm">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td>Null</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
