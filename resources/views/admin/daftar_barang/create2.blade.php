@extends('admin.layouts.master')

@section('title')
| Tambah Barang
@endsection

@section('mainJudul')
Tambah Barang
@endsection

@section('subJudul')

@endsection

@section('content1')
<form action="/daftar-barang" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group row">
        <label for="kode" class="col-2 col-form-label">Kode</label>
        <div class="col-10">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fa fa-address-book"></i>
                    </div>
                </div>
                <input id="kode" name="kode" type="text" class="form-control">
            </div>
            @error('kode')
            <div class="alert alert-warning small">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="gambar" class="col-2 col-form-label">Image</label>
        <div class="col-10">
            <div class="input-group">
                <input id="gambar" name="gambar" type="file" class="form-control">
            </div>
            @error('gambar')
            <div class="alert alert-warning small">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="nama_brg" class="col-2 col-form-label">Nama Barang</label>
        <div class="col-10">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fa fa-address-book"></i>
                    </div>
                </div>
                <input id="nama_brg" name="nama_brg" type="text" class="form-control">
            </div>
            @error('nama_brg')
            <div class="alert alert-warning small">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="harga" class="col-2 col-form-label">Harga</label>
        <div class="col-10">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fa fa-address-book"></i>
                    </div>
                </div>
                <input id="harga" name="harga" type="number" class="form-control">
            </div>
            @error('harga')
            <div class="alert alert-warning small">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="stok" class="col-2 col-form-label">Stok</label>
        <div class="col-10">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fa fa-address-book"></i>
                    </div>
                </div>
                <input id="stok" name="stok" type="number" class="form-control">
            </div>
            @error('stok')
            <div class="alert alert-warning small">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="textarea" class="col-2 col-form-label">Deskripsi</label>
        <div class="col-10">
            <textarea id="deskripsi" name="deskripsi" cols="40" rows="4" class="form-control"></textarea>
            @error('deskripsi')
            <div class="alert alert-warning small">{{ $message }}</div>
            @enderror
        </div>
    </div>
    </div>
    <div class="form-group row">
        <label for="penulis" class="col-2 col-form-label">Penulis</label>
        <div class="col-10">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fa fa-address-book"></i>
                    </div>
                </div>
                <input id="penulis" name="penulis" type="text" class="form-control">
            </div>
            @error('penulis')
            <div class="alert alert-warning small">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="penerbit" class="col-2 col-form-label">Penerbit</label>
        <div class="col-10">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fa fa-address-book"></i>
                    </div>
                </div>
                <input id="penerbit" name="penerbit" type="text" class="form-control">
            </div>
            @error('penerbit')
            <div class="alert alert-warning small">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="tanggal" class="col-2 col-form-label">Tanggal</label>
        <div class="col-10">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fa fa-address-book"></i>
                    </div>
                </div>
                <input id="tanggal" name="tanggal" type="date" class="form-control">
            </div>
            @error('tanggal')
            <div class="alert alert-warning small">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="kategori_id" class="col-2 col-form-label">Kategori</label>
        <div class="col-10">
            <select name="kategori_id" id="kategori_id" class="form-control">
                <option value="">---Pilih Kategori---</option>
                @foreach ($kategori as $item)
                <option value="{{ $item->id }}">{{ $item->nama }}</option>
                @endforeach

            </select>
            @error('kategori_id')
            <div class="alert alert-warning small">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="offset-2 col-9">
            <button name="submit" type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>
@endsection
