<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>E-ITb Admin @yield('title')</title>
    <link rel="stylesheet"
        href="{{asset('adminMaster/template/assets/vendors/mdi/css/materialdesignicons.min.css')}}" />
    <link rel="stylesheet"
        href="{{asset('adminMaster/template/assets/vendors/flag-icon-css/css/flag-icon.min.css')}}" />
    <link rel="stylesheet" href="{{asset('adminMaster/template/assets/vendors/css/vendor.bundle.base.css')}}" />
    <link rel="stylesheet"
        href="{{asset('adminMaster/template/assets/vendors/font-awesome/css/font-awesome.min.css')}}" />
    <link rel="stylesheet"
        href="{{asset('adminMaster/template/assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" />
    <link rel="stylesheet" href="{{asset('adminMaster/template/assets/css/style.css')}}" />
    <link rel="shortcut icon" type="image/png" href="{{ asset('user/assets/img/icon-tab.png') }}">
    @stack('dataTablesCSS')
</head>

<body>
    @include('alert.alert-form')
    <div class="container-scroller">

        {{-- Sidebar --}}
        @include('admin.partials.sidebar')
        {{-- /Sidebar --}}

        <div class="container-fluid page-body-wrapper">
            <div id="theme-settings" class="settings-panel">
                <i class="settings-close mdi mdi-close"></i>
                <p class="settings-heading">SIDEBAR SKINS</p>
                <div class="sidebar-bg-options selected" id="sidebar-default-theme">
                    <div class="img-ss rounded-circle bg-light border mr-3"></div> Default
                </div>
                <div class="sidebar-bg-options" id="sidebar-dark-theme">
                    <div class="img-ss rounded-circle bg-dark border mr-3"></div> Dark
                </div>
                <p class="settings-heading mt-2">HEADER SKINS</p>
                <div class="color-tiles mx-0 px-4">
                    <div class="tiles light"></div>
                    <div class="tiles dark"></div>
                </div>
            </div>

            {{-- Navbar --}}
            @include('admin.partials.navbar')
            {{-- /Navbar --}}

            <div class="main-panel">
                <div class="content-wrapper pb-0">
                    <div class="page-header flex-wrap">
                        <h3 class="mb-0"> @yield('mainJudul') <span
                                class="pl-0 h6 pl-sm-2 text-muted d-inline-block">@yield('subJudul')</span>
                        </h3>
                    </div>
                    <div class="row">
                        @yield('content1')
                    </div>
                    {{-- <div class="row">
                        @yield('content2')
                    </div> --}}
                </div>
                <footer class="footer">
                    <div class="d-sm-flex justify-content-center justify-content-sm-between">
                        <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright ©
                            bootstrapdash.com 2020</span>
                        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"> Free <a
                                href="https://www.bootstrapdash.com/" target="_blank">Bootstrap dashboard template</a>
                            from Bootstrapdash.com</span>
                    </div>
                </footer>
            </div>
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{asset('adminMaster/template/assets/vendors/js/vendor.bundle.base.js')}}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="{{asset('adminMaster/template/assets/vendors/chart.js/Chart.min.js')}}"></script>
    <script src="{{asset('adminMaster/template/assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js')}}">
    </script>
    <script src="{{asset('adminMaster/template/assets/vendors/flot/jquery.flot.js')}}"></script>
    <script src="{{asset('adminMaster/template/assets/vendors/flot/jquery.flot.resize.js')}}"></script>
    <script src="{{asset('adminMaster/template/assets/vendors/flot/jquery.flot.categories.js')}}"></script>
    <script src="{{asset('adminMaster/template/assets/vendors/flot/jquery.flot.fillbetween.js')}}"></script>
    <script src="{{asset('adminMaster/template/assets/vendors/flot/jquery.flot.stack.js')}}"></script>
    <script src="{{asset('adminMaster/template/assets/vendors/flot/jquery.flot.pie.js')}}"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{asset('adminMaster/template/assets/js/off-canvas.js')}}"></script>
    <script src="{{asset('adminMaster/template/assets/js/hoverable-collapse.js')}}"></script>
    <script src="{{asset('adminMaster/template/assets/js/misc.js')}}"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src="{{asset('adminMaster/template/assets/js/dashboard.js')}}"></script>
    @stack('dataTablesJS')
    <!-- End custom js for this page -->
</body>

</html>
