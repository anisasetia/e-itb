## Final Project
## Kelompok 15
## Anggota Kelompok
- Andika Dwiki Darmawan
- Annisa Yuli Setyaningsih
- Muhammad Ismail

## Tema Project
Book Store dengan konsep mini e-commerce

## ERD
![alt text](ERD.png)

## Link Video
Link Demo Aplikasi: https://youtu.be/yu5sqpuuZPk

Link Deploy: https://arcane-shelf-69223.herokuapp.com/

## Catatan :
- link admin = https://arcane-shelf-69223.herokuapp.com/admin (template Breeze)

- link user = https://arcane-shelf-69223.herokuapp.com/ (template Fruitkha)
