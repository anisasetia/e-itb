<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'UserController@index')->name('home');
Route::get('/about', 'UserController@about')->name('about');
Route::get('/product', 'UserController@product')->name('product');
Route::get('/cart', 'UserController@cart')->name('cart');
Route::put('batal/{cart}', 'UserController@batal')->name('user.batal');
Route::post('/product', 'UserController@buy')->name('user.buy');
Route::get('detail/{detail_id}', 'UserController@detail')->name('detail');


// Admin CRUD
Route::resource('admin', 'AdminController')->middleware('checkRole:1');;
// Barang CRUD
Route::resource('daftar-barang', 'BarangController')->middleware('checkRole:1');;
//Kategori CRUD
Route::resource('kategori', 'KategoriController')->middleware('checkRole:1');;
// Transaksi
Route::resource('transaksi', 'TransaksiController');
Route::get('/list-transaksi', 'TransaksiController@list_data')->name('list_data');
Route::get('/export-trans', 'ExportController@index')->name('export_trans');
//Akun
Route::resource('akun', 'AkunController');


Auth::routes();
