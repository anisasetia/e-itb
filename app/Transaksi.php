<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = 'barang_user';
    protected $guarded = [];
    protected $fillable = [
        'id',
        'kode_transaksi',
        'status',
        'bukti_bayar',
        'total_bayar',
        'qty',
        'user_id',
        'barang_id',
        'admin_id',

    ];

    public function barang()
    {
        return $this->belongsTo(Barang::class, 'barang_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
