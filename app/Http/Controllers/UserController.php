<?php

namespace App\Http\Controllers;

use App\Barang;
use App\Kategori;
use App\Transaksi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index()
    {
        $barang = Barang::find(1);

        // dd($barang->user);
        return view('user.page.index');
    }

    public function about()
    {
        return view('user.page.about');
    }

    public function product(Barang $ebooks)
    {
        $kategoris = Kategori::all();
        $ebooks = Barang::all();
        // $now = Carbon::now();
        // $day = date("Y-m-d");
        // $tanggal = $now->year . $now->month . $now->day;
        // $cek = Transaksi::count();
        // $no_trans = Transaksi::all()->last();

        // if ($cek == 0) {
        //     $urut = 1001;
        //     $nomor = $tanggal . $urut;
        // } elseif ($no_trans->tanggal_transaksi < $day) {
        //     $nomor = $tanggal . 1001;
        // } elseif (substr($no_trans->tanggal_transaksi, -4) == 1999) {
        //     $nomor = $tanggal . 1001;
        // } else {
        //     $ambil = Transaksi::all()->last();
        //     $urut = substr($ambil->kode_transaksi, -4) + 1;
        //     $nomor = $tanggal . $urut;
        // }

        return view('user.page.product', compact('ebooks', 'kategoris'));
    }

    public function cart()
    {
        $cart = Transaksi::select(
            "kode_transaksi",

            "status",
            "total_bayar",
            "qty",
            "barangs.nama_brg as barang",
            "users.id as user",
        )
            ->join("barangs", "barangs.id", "=", "barang_user.barang_id")
            ->join("users", "users.id", "=", "barang_user.user_id")
            ->where("users.id", "=", Auth::user()->id)
            ->get();
        // dd($cart);
        return view('user.page.cart', compact('cart'));
    }

    public function keranjang()
    {

        return view('user.page.product');
    }

    public function buy(Request $request, Transaksi $transaksi)
    {

        $transaksi->kode_transaksi = $request->kode_transaksi;
        $transaksi->status = $request->status;
        $transaksi->total_bayar = $request->total_bayar;
        $transaksi->qty = $request->qty;
        $transaksi->barang_id = $request->barang_id;
        $transaksi->bukti_bayar = $request->bukti_bayar;
        $transaksi->user_id = $request->akun_id;

        $transaksi->save();

        return redirect('/cart');
    }
    public function detail($id)
    {
        $transaksi = Transaksi::all();

        $now = Carbon::now();
        $day = date("Y-m-d");
        $tanggal = $now->year . $now->month . $now->day;
        $cek = Transaksi::count();
        $no_trans = Transaksi::all()->last();

        if ($cek == 0) {
            $urut = 1001;
            $nomor = $tanggal . $urut;
        } elseif ($no_trans->created_at < $day) {
            $nomor = $tanggal . 1001;
        } elseif (substr($no_trans->kode_transaksi, -4) == 1999) {
            $nomor = $tanggal . 1001;
        } else {
            $ambil = Transaksi::all()->last();
            $urut = substr($ambil->kode_transaksi, -4) + 1;
            $nomor = $tanggal . $urut;
        }
        $detail = Barang::findOrFail($id);
        // dd($nomor);

        return view('user.page.detail', compact('detail', 'nomor', 'transaksi'));
    }

    public function batal(Request $request, $id)
    {
        $transaksi = Transaksi::findOrFail($id);

        $transaksi->update([
            'status' => $request->status,
        ]);
        $transaksi->status = $request->status;

        session()->flash('delete_cart', 'Gagal');
        return redirect('/cart');
    }
}
