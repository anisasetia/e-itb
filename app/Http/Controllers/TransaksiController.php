<?php

namespace App\Http\Controllers;

use Yajra\Datatables\Datatables;
use App\Transaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $transaksi = Transaksi::select(
        //     "kode_transaksi",
        //     "status",
        //     "total_bayar",
        //     "qty",
        //     "barangs.nama_brg as barang",
        //     "users.id as user",
        // )
        //     ->join("barangs", "barangs.id", "=", "barang_user.barang_id")
        //     ->join("users", "users.id", "=", "barang_user.user_id")
        //     ->get();

        $auth = Auth::user()->id;
        $transaksi = Transaksi::where('user_id', $auth)->get()
            ->map(function ($item, $value) {
                return [
                    'id' => $item->id,
                    'user' => $item->user->name,
                    'kode_barang' => $item->barang->kode,
                    'kode_transaksi' => $item->kode_transaksi,
                    'qty' => $item->qty,
                    'total_bayar' => $item->total_bayar,
                    'status' => $item->status,
                ];
            });
        // dd($transaksi);
        // return response($transaksi);
        return view('admin.transaksi.index');
    }

    public function list_data()
    {
        $auth = Auth::user()->id;
        $transaksi = Transaksi::all()
            ->map(function ($item, $value) {
                return [
                    'id' => $item->id,
                    'user' => $item->user->name,
                    'nama_barang' => $item->barang->nama_brg,
                    'kode_barang' => $item->barang->kode,
                    'kode_transaksi' => $item->kode_transaksi,
                    'qty' => $item->qty,
                    'total_bayar' => $item->total_bayar,
                    'status' => $item->status,
                ];
            });

        return DataTables::of($transaksi)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $transaksi = Transaksi::find($id);

        $transaksi->status = "lunas";

        $transaksi->save();

        return response()->json($transaksi);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaksi = Transaksi::find($id);

        $transaksi->delete();
        session()->flash('delete_cart', 'Berhasil');
        return response()->json($transaksi);
    }
}
