<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Barang;
use App\Kategori;
use File;

class BarangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index','show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barang = Barang::all();

        return view('admin.daftar_barang.index', compact('barang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $barang = Barang::all();
        $kategori = Kategori::all();
        return view('admin.daftar_barang.create', compact('kategori', 'barang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kode' => 'required',
            'gambar' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'nama_brg' => 'required',
            'harga' => 'required',
            'stok' => 'required',
            'deskripsi' => 'required',
            'penulis' => 'required',
            'penerbit' => 'required',

        ]);

        $input = $request->all();

        $path = $request->file('gambar')->store('public/image');
        $input['gambar'] = $path;

        Barang::create($input);
        session()->flash('success', 'Berhasil');
        return redirect('/daftar-barang');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $barang = Barang::findOrFail($id);

        return view('admin.daftar_barang.show', compact('barang'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = Kategori::all();
        $barang = Barang::findOrFail($id);

        return view('admin.daftar_barang.edit', compact('barang', 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kode' => 'required',
            'gambar' => 'image|mimes:jpeg,png,jpg|max:2048',
            'nama_brg' => 'required',
            'harga' => 'required',
            'stok' => 'required',
            'deskripsi' => 'required',
            'penulis' => 'required',
            'penerbit' => 'required',
            'tanggal' => 'required',
        ]);

        $barang = Barang::find($id);
        if (request()->file('gambar')) {
            \Storage::delete($barang->gambar);
            $path = $request->file('gambar')->store('public/image');
        } else {
            $path = $barang->gambar;
        }

        $barang->gambar = $path;
        $barang->nama_brg = $request->nama_brg;
        $barang->kode = $request->kode;
        $barang->harga = $request->harga;
        $barang->stok = $request->stok;
        $barang->deskripsi = $request->deskripsi;
        $barang->penulis = $request->penulis;
        $barang->penerbit = $request->penerbit;
        $barang->tanggal = $request->tanggal;

        $barang->save();

        return redirect('/daftar-barang');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $barang = Barang::find($id);
        \Storage::delete($barang->gambar);

        $barang->delete();
        session()->flash('delete_berhasil', 'Gagal');

        return redirect('/daftar-barang');
    }
}
