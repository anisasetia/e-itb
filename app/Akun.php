<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Akun extends Model
{
    protected $table = 'akun';

    protected $fillable = ['username', 'pswd', 'pelanggan_id'];

    public function barang()
    {
        $this->belongsToMany(Barang::class, 'transaksi');
    }
}
