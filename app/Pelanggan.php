<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelanggan extends Model
{
    protected $table = 'pelanggan';
    protected $fillable = [
        'alamat',
        'no_hp',
        'gender',
        'image',
        'users_id'
    ];

    public function pelanggan()
    {
        return $this->hasMany('App\Pelanggan');
    }
}

