<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = 'barangs';
    protected $fillable = ['kode', 'gambar', 'nama_brg', 'harga', 'stok', 'deskripsi', 'penulis', 'penerbit', 'tanggal', 'kategori_id'];

    public function kategori()
    {
        return $this->belongsTo('App\Kategori');
    }


    public function user()
    {
        return $this->belongsToMany('App\User');
    }
}
